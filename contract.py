# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateView, Wizard


class Contract(metaclass=PoolMeta):
    __name__ = 'staff.contract'
    project = fields.Many2One('project.work', 'Project',
        domain=[('type', '=', 'project')])

    @classmethod
    def __setup__(cls):
        super(Contract, cls).__setup__()


class ProjectCollectiveReport(Report):
    "Project - Collective Report"
    __name__ = 'staff_project.collective_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        return report_context


class ProjectHistoryReport(Report):
    "Project - History Report"
    __name__ = 'staff_project.history_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        return report_context


class ProjectDiscountReport(Report):
    "Project - Disccount Report"
    __name__ = 'staff_project.disccount_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        return report_context


class ProjectNoCertificationReport(Report):
    "Project - No Certification Report"
    __name__ = 'staff_project.no_certification_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        return report_context


class ProjectEmployeeContractReport(Report):
    "Project - Employee Contract Report"
    __name__ = 'staff_project.project_employee_contract_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        return report_context


class ContractingFicStart(ModelView):
    "Contracting Fic Start"
    __name__ = 'staff_contracting.fic.start'
    start_period = fields.Many2One('staff.payroll.period',
            'Start Period', required=True)
    end_period = fields.Many2One('staff.payroll.period', 'End Period',
            depends=['start_period'])
    company = fields.Many2One('company.company', 'Company', required=True)
    project = fields.Many2One('project.work', 'Project',
        domain=[('type', '=', 'project')])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('start_period')
    def on_change_with_end_period(self, name=None):
        if self.start_period:
            return self.start_period.id


class ContractingFic(Wizard):
    "Contracting Fic"
    __name__ = 'staff_contracting.fic'
    start = StateView('staff_contracting.fic.start',
        'staff_contracting.contracting_fic_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('staff_contracting.fic_report')

    def do_print_(self, action):
        end_period_id = None
        department_id = None
        project_id = None
        if self.start.end_period:
            end_period_id = self.start.end_period.id
        if self.start.project:
            project_id = self.start.project.id
        data = {
            'ids': [],
            'company': self.start.company.id,
            'start_period': self.start.start_period.id,
            'end_period': end_period_id,
            'project': project_id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ContractingFicReport(Report):
    "Contracting Fic Report"
    __name__ = 'staff_contracting.fic_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        Payroll = Pool().get('staff.payroll')
        Period = Pool().get('staff.payroll.period')
        Project = Pool().get('project.work')
        Configuration = Pool().get('staff.configuration')
        config = Configuration.search([])
        dom = []
        start_date = None
        minimum_salary = 0
        end_date = None
        period = None
        period_name = None
        project = None
        start_period = Period(data['start_period'])
        if data['end_period']:
            end_period = Period(data['end_period'])
            start_date = start_period.start
            end_date = end_period.end
            dom = [
            ('start', '>=', start_date),
            ('end', '<=', end_date),
            ]
            period_start = start_period.name
            period_end = end_period.name
        else:
            dom = [
            ('start', '>=', start_date),
            ]
            period_start = start_period.name
            period_end = period_start.name
        if data['project']:
            dom.append(('project.id', '=', data['project']))
            project = Project(data['project']).name
        dom.append(('contract.builder_employee', '=', 'yes'))
        payrolls = Payroll.search(dom,
        order=[('employee.party.name', 'ASC')],
        )
        lines_dom = []
        party_ids = []
        total_aport = []
        num = 0
        aport = 0
        if config[0].minimum_salary:
            minimum_salary = config[0].minimum_salary
        for payroll in payrolls:
            party_id = payroll.employee.party.id
            if party_id in party_ids:
                continue
            num = num + 1
            if payroll.employee.party.type_document:
                if payroll.employee.party.type_document == '13':
                    type_document = 'Cédula de ciudadanía'
                elif payroll.employee.party.type_document == '11':
                    type_document = 'Registro Civil de Nacimiento'
                elif payroll.employee.party.type_document == '12':
                    type_document = 'Tarjeta de Identidad'
                elif payroll.employee.party.type_document == '21':
                    type_document = 'Tarjeta de Extranjeria'
                elif payroll.employee.party.type_document == '22':
                    type_document = 'Cedula de Extranjeria'
                elif payroll.employee.party.type_document == '31':
                    type_document = 'Nit'
                elif payroll.employee.party.type_document == '41':
                    type_document = 'Pasaporte'
                elif payroll.employee.party.type_document == '42':
                    type_document = 'Tipo de Documento Extranjero'
                elif payroll.employee.party.type_document == '43':
                    type_document = 'Sin identificacion del Exterior o para uso definido por la DIAN'
                else:
                    type_document = 'None'

            payroll.type_document = type_document
            payroll.num = num
            lines_dom.append(payroll)
            party_ids.append(party_id)
            aport = (minimum_salary / 40)
            payroll.aport = aport
            total_aport.append(aport)
            # if num < 40:
            #     aport = minimum_salary / num
            # if num % 40 == 0:
            #     print('hay 40')
            #     aport_ = minimum_salary / 40
            #     for line in lines_dom:
            #         line.aport = aport_
            # res = num % 40
            # for line in lines_dom:
            #     total_aport.append(line.aport)
            # if line.aport == 0:
            #     print('este si: ', line)
            #     aport = minimum_salary / res
            #     line.aport = aport

        report_context['records'] = lines_dom
        report_context['minimum_salary'] = minimum_salary
        report_context['project'] = project
        report_context['total_aport'] = sum(total_aport)
        report_context['company'] = user.company
        report_context['user'] = user
        report_context['start'] = period_start
        report_context['end'] = period_end
        return report_context
